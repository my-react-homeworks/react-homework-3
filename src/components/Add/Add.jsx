import React from "react";
import "./Add.css";

class Add extends React.Component {
    state = {
        inputValue: "",
        isInputOpen: false,
    };

    openInput = () => {
        this.setState((state) => {
            return {
                ...state,
                isInputOpen: true,
            };
        });
    };

    closeInput = () => {
        this.setState((state) => {
            return {
                ...state,
                inputValue: "",
                isInputOpen: false,
            };
        });
    };

    changeValue = (value) => {
        this.setState((state) => {
            return {
                ...state,
                inputValue: value,
            };
        });
    };

    addItem = () => {
        if (this.state.inputValue !== ""){
            this.props.add(this.state.inputValue);
            this.closeInput();
        }
        else{
            document.querySelector(".input input").style.border = "1px solid red";
        }
    }

    render() {
        return (
            <>
                <button
                    className="btn add-btn"
                    type="button"
                    onClick={this.state.isInputOpen === true ? this.closeInput : this.openInput}
                >
                    Add Item <i className="fa-solid fa-plus"></i>
                </button>
                {this.state.isInputOpen ? (
                    <div className="input">
                        <input
                            autoFocus
                            type="text"
                            value={this.state.inputValue}
                            onChange={(e) => this.changeValue(e.target.value)}
                        />
                        <button
                            className="btn save-btn"
                            type="button"
                            onClick={() => this.addItem()}
                        >
                            Save <i className="fa-regular fa-floppy-disk"></i>
                        </button>
                    </div>
                ) : null}
            </>
        );
    }
}

export default Add;
