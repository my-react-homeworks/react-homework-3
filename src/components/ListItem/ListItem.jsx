import React from "react";
import './ListItem.css';

function ListItem({value, change}){
    let oldValue = value.name;
    return (
        <li className="list-item">
            <span className={value.isComplete ? "complete" : ""}>{value.name}</span>
            <button type="button" className="item-btn" onClick={(e) => {
                    const span = e.currentTarget.previousElementSibling;
                    span.setAttribute("contenteditable", true);
                    span.setAttribute("spellcheck", false);
                    span.focus();
                    span.addEventListener("blur", (e) => {
                        let newValue = span.innerText.trim();
                        change.editItem(oldValue, newValue);
                        span.removeAttribute("contenteditable");
                    })
                }}>
                <i className="fa-solid fa-pen-to-square"></i>
            </button>
            <button type="button" className="item-btn" onClick={() => {change.deleteItem(value.name)}}>
                <i className="fa-solid fa-trash"></i>
            </button>
            <button type="button" className="item-btn" onClick={() => {change.completeItem(value.name)}}>
                <i className="fa-solid fa-check-double"></i>
            </button>
        </li>
    );
}

export default ListItem;