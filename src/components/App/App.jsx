import React from "react";
import List from '../List/List'
import Add from "../Add/Add";

class App extends React.Component {
    state = {
        shoppingList: localStorage.shoppingList
            ? JSON.parse(localStorage.getItem("shoppingList"))
            : [],
    };

    addProduct = (product) => {
        this.setState((state) => {
            return {
                ...state,
                shoppingList: [...this.state.shoppingList, { name: product, isComplete: false }],
            };
        });
    };

    changeList = {
        editItem: (oldValue, newValue) => {
            this.setState(state => {
                return{
                    ...state,
                    shoppingList: this.state.shoppingList.map(el => {
                        if(el.name === oldValue){
                            el.name = newValue;
                        }
                        return el;
                    })
                }
            })
        },
        deleteItem: (value) => {
            this.setState(state => {
                return {
                    ...state,
                    shoppingList: this.state.shoppingList.filter(el => el.name !== value)
                }
            })
        },
        completeItem: (value) => {
            this.setState(state => {
                return {
                    ...state,
                    shoppingList: this.state.shoppingList.map(el => {
                        if(el.name === value){
                            el.isComplete = true;
                        }
                        return el;
                    })
                }
            })
        }
    };

    componentDidUpdate() {
        localStorage.setItem(
            "shoppingList",
            JSON.stringify(this.state.shoppingList)
        );
    }

    render() {
        return (
            <div className="wrapper">
                <h2>Shopping List</h2>
                <Add add={this.addProduct} />
                <List list={this.state.shoppingList} change={this.changeList} />
                <div className="copyright">
                    &copy; Development by{" "}
                    <a href="mailto:t.info@ukr.net">Anton Biletskyi</a>
                </div>
            </div>
        );
    }
}

export default App;