import React from "react";
import ListItem from '../ListItem/ListItem'
import './List.css'

function List({list, change}){
    let idCounter = 1;
    return (
        <ul className="list">
            {list.map(el => {
                return <ListItem value={el} key={el + idCounter++} change={change}/>
            })}
        </ul>
    );
}

export default List;