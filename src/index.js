import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App/App';
import './normalize.css';
import './style.css';

ReactDOM.render(<App />, document.querySelector("#root"));